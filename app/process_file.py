import sys, os, sqlite3

#processing steps:
# 1. create batch record based on file/date
# 2. read file data into 'raw' tables
# 3. migrate data to 'final' table

## correctness is assumed but verification steps stubbed for bonus points
## sql inserts should be optimized if files are of consequential size

## database path and source file path can be overriden with the
## MEMBER_DB and MEMBER_SOURCE environment veriables respectively
def processdata(path, db):
	
	(table, createddate) = parsefilename(path)

	batch_id = create_batch(db, createddate, path)

	err = verify_table(table)

	log_batch_error(batch_id, err)

	h = open(path, 'r')

	columns = h.readline().strip().split("|")

	err = verify_columns(columns)

	log_batch_error(batch_id, err)

	while(True):
		line = h.readline()
		if not line:
			break
		load_data(db, [batch_id] + 
			list(map(lambda x: None if x == "" else x, line.strip().split("|"))))
	
	validate_data(db, batch_id)

def db_connect(connection_string):
	return sqlite3.connect(connection_string)

def parsefilename(path):
	name = os.path.basename(path).split('.')
	return (name[0], name[1])

def create_batch(db, batchdate, filepath):
	sql = """
		INSERT INTO member_eligibility_batches (
			BATCH_DATE,
			SOURCE_FILE
		) VALUES (
			?,
			?
		);
	"""
	return do_insert(db, sql, [batchdate, filepath]) 


def check_table(db, name):
	q = "SELECT 1 from sqlite_master where type='table' and name=?"
	return db.execute(query, [name]).fetchone() is not None

def verify_table(tablename):
	print("Assume the table name is correct")
	return None

def verify_columns(columns):
	print("Assume the column names are correct")
	return None

def log_batch_error(bid, err):
	print("assuming no errors")
	if err is None:
		return None


def load_data(db, data):
	sql = """
		INSERT INTO member_eligibility_batch_data (
			BATCH_ID,
			MEMBER_ID,
			FIRST_NAME,
			LAST_NAME,
			ELIGIBILITY_START,
			ELIGIBILITY_END
		) VALUES (
			?,
			?,
			?,
			?,
			?,
			?
		);
	"""

	return do_insert(db, sql, data)

def validate_data(db, batch_id):
	sql = ["""
		--do the buisness logic validation here
		--we are assuming everything is correct
		select ?
		""",
		"""
		insert into member_eligibility (
			MEMBER_ID,
			FIRST_NAME,
			LAST_NAME,
			ELIGIBILITY_START,
			ELIGIBILITY_END
		)
		select 
			MEMBER_ID,
			upper(FIRST_NAME), --by convention names should be stored as upper
			upper(LAST_NAME),
			ELIGIBILITY_START,
			ELIGIBILITY_END
		from member_eligibility_batch_data d
		where 
			d.batch_id = ?
		on conflict(MEMBER_ID)
		do update
			set ELIGIBILITY_END = excluded.ELIGIBILITY_END
			where excluded.ELIGIBILITY_END is not null
			and ELIGIBILITY_END is null
		"""]

	for s in sql:
		do_insert(db, s, [batch_id])


def do_insert(db, sql, data):
	cur = db.cursor()
	cur.execute(sql, data)
	db.commit()
	return cur.lastrowid


if __name__ == "__main__":
	dbpath = os.environ.get("MEMBER_DB") or "/etc/member-eligibility/db/db.sqlite"
	sourcepath = os.environ.get("MEMBER_SOURCE") or "/etc/member-eligibility/source/"
	db = db_connect(dbpath)
	processdata(f'{sourcepath}{sys.argv[1]}', db)

